import os
import subprocess
import sys

from datetime import timedelta
from airflow.decorators import task
from airflow import DAG
#from rabbitmq_provider.operators.rabbitmq import RabbitMQOperator
from airflow.providers.amazon.aws.operators.emr import EmrAddStepsOperator
from airflow.providers.amazon.aws.operators.emr import EmrCreateJobFlowOperator
from airflow.providers.amazon.aws.sensors.emr import EmrStepSensor
from airflow.utils.dates import days_ago


def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

install('apache-airflow[amazon]')

DAG_ID = os.path.basename(__file__).replace('.py', '')

DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
}

SPARK_STEPS = [
    {
        'Name': 'bronze',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': ['spark-submit',
                     '--master', 'yarn',
                     '--deploy-mode', 'cluster',
                     '--packages', 'io.delta:delta-core_2.12:2.2.0',
                     '--conf', 'spark.sql.shuffle.partitions=200' ,                     
                     '--conf', 'spark.sql.extensions=io.delta.sql.DeltaSparkSessionExtension',
                     '--conf', 'spark.sql.catalog.spark_catalog=org.apache.spark.sql.delta.catalog.DeltaCatalog',
                     's3://shapes-artifactory/bronce_test.py'],
        },
    },
    {
        'Name': 'silver',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': ['spark-submit',
                     '--master', 'yarn',
                     '--deploy-mode', 'cluster',
                     '--packages', 'io.delta:delta-core_2.12:2.2.0',
                     '--conf', 'spark.sql.shuffle.partitions=200' ,                     
                     '--conf', 'spark.sql.extensions=io.delta.sql.DeltaSparkSessionExtension',
                     '--conf', 'spark.sql.catalog.spark_catalog=org.apache.spark.sql.delta.catalog.DeltaCatalog',
                     's3://shapes-artifactory/silver_test.py'],
        },
    },
    {
        'Name': 'enriched',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': ['spark-submit',
                     '--master', 'yarn',
                     '--deploy-mode', 'cluster',
                     '--packages', 'io.delta:delta-core_2.12:2.2.0',
                     '--conf', 'spark.sql.shuffle.partitions=200' ,                     
                     '--conf', 'spark.sql.extensions=io.delta.sql.DeltaSparkSessionExtension',
                     '--conf', 'spark.sql.catalog.spark_catalog=org.apache.spark.sql.delta.catalog.DeltaCatalog',
                     's3://shapes-artifactory/enriched_test.py'],
        },
    }
]

JOB_FLOW_OVERRIDES = {
    'Name': 'demo-cluster-airflow',
    'ReleaseLabel': 'emr-6.10.0',
    'LogUri': 's3://airflow-logs-shapes/',
    'Applications': [
        {
            'Name': 'Spark'
        },
    ],
    'Instances': {
        'InstanceGroups': [
            {
                'Name': "Master nodes",
                'Market': 'SPOT',
                'InstanceRole': 'MASTER',
                'InstanceType': 'm4.large',
                'InstanceCount': 1,
            },
            {
                'Name': "Slave nodes",
                'Market': 'SPOT',
                'InstanceRole': 'CORE',
                'InstanceType': 'm4.large',
                'InstanceCount': 2,
            }
        ],
        'KeepJobFlowAliveWhenNoSteps': False,
        'TerminationProtected': False,
    },
    'VisibleToAllUsers': True,
    'JobFlowRole': 'EMR_EC2_DefaultRole',
    'ServiceRole': 'EMR_DefaultRole',
    'Tags': [
        {
            'Key': 'Environment',
            'Value': 'Development'
        },
        {
            'Key': 'Name',
            'Value': 'Airflow EMR Demo Project'
        },
        {
            'Key': 'Owner',
            'Value': 'Data Analytics Team'
        }
    ]
}

with DAG(
        dag_id=DAG_ID,
        description='Run built-in Spark app on Amazon EMR',
        default_args=DEFAULT_ARGS,
        dagrun_timeout=timedelta(hours=2),
        start_date=days_ago(1),
        schedule_interval='@once',
        tags=['emr'],
) as dag:
    cluster_creator = EmrCreateJobFlowOperator(
        task_id='create_job_flow',
        job_flow_overrides=JOB_FLOW_OVERRIDES
    )

    bronze = EmrAddStepsOperator(
        task_id='add_steps',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='create_job_flow', key='return_value') }}",
        #job_flow_id='j-3571JMT4K285S',
        aws_conn_id='aws_default',
        steps=SPARK_STEPS,
    )
    last_step = len(SPARK_STEPS) - 1
    bronze_checker = EmrStepSensor(
        task_id='bronze_step',
        job_flow_id="{{ task_instance.xcom_pull('create_job_flow', key='return_value') }}",
        step_id="{{ task_instance.xcom_pull(task_ids='add_steps', key='return_value')["
        + str(last_step)
        + "] }}",
        aws_conn_id='aws_default',
    )

    @task
    def my_task():
        var = "test"  # this is fine, because func my_task called only run task, not scan dags.
        print(var)


# Set up the DAG dependencies
#rabbitmq_op >> emr_op >> add_steps_op
cluster_creator >> bronze >> bronze_checker 
#step_adder