# Filename: hello_world2.py
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
import pika



default_args = {
  'owner': 'airflow',
  'depends_on_past': False,
  'start_date': datetime(2019, 9, 9),
  'email': ['airflow@example.com'],
  'email_on_failure': False,
  'email_on_retry': False,
  'retries': 1,
  'retry_delay': timedelta(minutes=5),
# 'queue': 'bash_queue',
# 'pool': 'backfill',
# 'priority_weight': 10,
# 'end_date': datetime(2016, 1, 1),
}

dag = DAG(  'hello_world2',
        schedule_interval='0 0 * * *' ,
                catchup=False,
        default_args=default_args
    )

class Publisher:
      def __init__(self, config):
          self.config = config
      def publish(self, routing_key, message):       
          connection = self.create_connection()
          # Create a new channel with the next available channel number or pass in a channel number to use 
          channel =connection.channel()
          # Creates an exchange if it does not already exist, and if the exchange exists,
          # verifies that it is of the correct and expected class.
          channel.exchange_declare(exchange=self.config['exchange'],
          exchange_type='topic')       
          #Publishes message to the exchange with the given routing key
          channel.basic_publish(exchange=self.config['exchange'],
          routing_key=routing_key, body=message)
          print("[x] Sent message %r for %r" % (message,routing_key))  
      # Create new connection
      def create_connection(self):
          param = pika.ConnectionParameters(host=self.config['host'],port=self.config['port']) 
          return pika.BlockingConnection(param)
#publisher.py
config = { 'host': '172.17.0.1','port': 5672, 'exchange' : 'my_exchange'}
publisher = Publisher(config)
publisher.publish('nse.nifty', 'New Data')
  